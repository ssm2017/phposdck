# PhpOsDck

[![N|OpenSimulator](http://opensimulator.org/images/d/d4/Os_b_200x50_r.png)](http://opensimulator.org)

## What is PhpOsDck ?
This script is a kind of http API to manage OpenSimulator simulators Docker images.

## Why creating PhpOsDck ?
The goal is to manage the [FrancoGrid](http://francogrid.org) simulators using this api with a Drupal website.

## Why not use the Docker web API directly ?
We need to create some folders outside of the container to be able to keep/update the config even when container is stopped and the Docker API is not made for that (or i do not know it well).

## How to install ?
  - Read the "Readme.md" file located in the "installation" folder.
  - Rename the file : "config/config.ini.example" to "config/config.ini" and modify it to your needs.

## How to use it ?
Here is the Api reference doc :

  - [Create a new simulator](#create-a-new-simulator)
  - [Get simulator infos](#get-simulator-infos)
  - [Delete simulator](#delete-simulator)
  - [Get all simulators infos](#get-all-simulators-infos)
  - [Start/Restart/Stop/Kill simulator](#startrestartstopkill-simulator)
  - [Get logs of simulator](#get-logs-of-simulator)
  - [Purge broken simulators](#purge-broken-simulators)

### Create a new simulator

#### Request
  - **Method** : POST
  - **Url** : ```/simulators```
  - **Body** : The body is a JSON associative array containing :
    - OwnerName : (string) The owner name of the simulator
    - OwnerUuid : (uuid) The owner uuid of the simulator
    - IsInteractive : (boolean) (default FALSE) Set if the container should be "interactive"
    - IsStandalone : (boolean) (default TRUE) Set if the simulator is using the standalone mode
    - IsHypergrid : (boolean) (default TRUE) Set if the simulator is using HyperGrid
    - GridName : If the simulator is not a standalone one, set the grid name (the grid name depends on the available grid choice (see "getAvailableGrids"))
    - OpenSim.ini : (associative array) This array contains OpenSim.ini values that can be set.
    - GridCommon.ini : (associative array) This array contains GridCommon.ini values that can be set (if the GridName is set to "custom")

#### Return
  - **If success** : code 200 and a JSON object containing :
    - sim_uuid : The uuid assigned to the simulator
    - sim_port : The port assigned to the simulator
  - **If error** : code depending on the error (400 or 500)

#### Example
Here is an example of the body sent to the server :
```
{
    "OwnerName": "toto titi",
    "OwnerUuid": "16830b9c-2169-4477-b186-ab173853d354",
    "IsInteractive": false,
    "IsStandalone" : true,
    "IsHypergrid" : true,
    "Grid" : "abcd",
    "OpenSim.ini": {
        "Startup": {
            "regionload_webserver_url": "http://mywebsite.com/grid/webregions/2208/xml"
        },
        "RemoteAdmin": {
            "access_password": "1234"
        },
        "Estates": {
            "DefaultEstateName": "mysuperestate",
            "DefaultEstateOwnerName": "toto titi",
            "DefaultEstateOwnerUUID": "16830b9c-2169-4477-b186-ab173853d354",
            "DefaultEstateOwnerEMail": "a@b.com",
            "DefaultEstateOwnerPassword": "1234"
        }
    }
}
```

### Get simulator infos

#### Request
  - **Method** : GET
  - **Url** : ```/simulators/<UUID>```
    - ```<UUID>``` is the simulator's uuid

#### Return
  - **If success** : code 200 and a json array containing :
    - Uuid
    - Port
    - OwnerName
    - OwnerUuid
  - **If error** : code depending on the error (400 or 404 or 500)

### Delete simulator

#### Request
  - **Method** : DELETE
  - **Url** : ```/simulators/<UUID>```
    - ```UUID>``` is the simulator's uuid

### Return
  - **If success** : code 200 and a json array containing :
    - message
  - **If error** : code depending on the error (400 or 404 or 500)
 
### Get all simulators infos

#### Request
  - **Method** : GET
  - **Url** : ```/simulators```

#### Return
  - **If success** : code 200 and a json array containing every simulator with these fields :
    - Uuid
    - Port
    - OwnerName
    - OwnerUuid
  - **If error** : code depending on the error (400 or 404 or 500)

### Start/Restart/Stop/Kill simulator

#### Request
  - **Method** : GET
  - **Url** : ```/simulators/<UUID>/<action>```
    - ```<UUID>``` is the simulator's uuid
    - ```<action>``` can be ```start```, ```restart```, ```stop```, ```kill```

#### Return
  - **If success** : code 200 and a json array containing a confirmation message.
  - **If error** : code depending on the error (400 or 404 or 500)

### Get logs of simulator

#### Request
  - **Method** : GET
  - **Url** : ```/simulators/<UUID>/logs
  - **Fields** :
    - ```stdout``` ((boolean)Default = "TRUE" (Select if you want to see the stdout))
    - ```stderr``` ((boolean)Default = "FALSE" (Select if you want to see the stderr))
    - ```tail``` ((integer)Default = 10 (Select the number of lines you want to see)
    - ```timestamps``` ((boolean)Default = "TRUE" (Select if you want to see the timestamps)
    - ```since``` ((integer)Default = 0 (Timestamp to get the logs since the defined time)

#### Return
  - **If success** : code 200 and a json array containing :
    - logs (object containing 2 arrays : "stdout" and "stderr")
    - message (string)
  - **If error** : code depending on the error (400 or 404 or 500)

### Purge broken simulators
  - This url is checking if a container has a corresponding folder.
    - If no, it is deleting the container
  - then it is checking if a folder has a corresponding container.
    - If no, it is deleting the folder.

#### Request
  - **Method** : GET
  - **Url** : ```/purge/all```

#### Return
  - **If success** : code 200 and a json array containing :
    - "purge by containers" (contains infos about containers)
    - "purge by folder" (contains infos about folders)
    - other messages if one simulator was deleted
  - **If error** : code depending on the error (400 or 404 or 500)