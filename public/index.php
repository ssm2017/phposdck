<?php

require '../vendor/autoload.php';

use Klein\Klein;
use Klein\Request;
use Klein\Response;
use Klein\ServiceProvider;

// store config
global $config;
$config = [];

// get the config
try
{
    \Phposdck\Utils::getConfig();
} catch (\Exception $e)
{
    $res = new Response();
    $code = $e->getCode();
    if ($code)
    {
        $res->code($code);
    } else
    {
        $res->code(400);
    }

    $res->json([
        'message' => $e->getMessage()
    ]);
    die();
}

// create the route
$klein = new Klein();

$klein->respond('GET', '/purge/all', function (Request $req, Response $res)
{
    $simulator_manager = new \Phposdck\SimulatorManager();
    $res->json($simulator_manager->purgeSimulators());
});

$klein->respond('GET', '/simulators/[:uuid]/[:action]', function(Request $req, Response $res)
{
    // get values
    $uuid = $req->param('uuid', NULL);

    // check action
    $action = $req->param('action', NULL);
    if (is_null($action) || !in_array($action, ['start', 'stop', 'restart', 'kill', 'logs']))
    {
        $res->code(400);
        $res->json([
            'details' => 'Wrong action'
        ]);
        return;
    }

    // do the action
    $uuid = $req->param('uuid', NULL);
    $simulator = new \Phposdck\Simulator();
    $simulator->setUuid($uuid);
    $simulator_manager = new \Phposdck\SimulatorManager();
    $simulator_manager->setSimulator($simulator);
    $answer = [];
    try
    {
        switch ($action)
        {
            case 'start':
                $simulator_manager->startSimulatorContainer();
                break;
            case 'stop':
                $simulator_manager->stopSimulatorContainer();
                break;
            case 'restart':
                $simulator_manager->restartSimulatorContainer();
                break;
            case 'kill':
                $simulator_manager->killSimulatorContainer();
                break;
            case 'logs':
                $config = [
                    'stdout' => $req->param('stdout', TRUE),
                    'stderr' => $req->param('stderr', FALSE),
                    'tail' => $req->param('tail', 10),
                    'timestamps' => $req->param('timestamps', TRUE),
                    'since' => $req->param('since', 0)
                ];
                $answer['logs'] = $simulator_manager->getSimulatorContainerLogs($config);
                break;
        }
        $answer['message'] = 'Action ' . $action . ' done on simulator ' . $uuid;
        $res->json([$answer]);
        //return;
    } catch (\Exception $e)
    {
        $code = $e->getCode();
        if ($code)
        {
            $res->code($code);
        } else
        {
            $res->code(400);
        }

        $res->json([
            'message' => $e->getMessage()
        ]);
    }
});

$klein->respond('GET', '/simulators/[:uuid]', function(Request $req, Response $res)
{
    try
    {
        $simulator = new \Phposdck\Simulator();
        $simulator->setUuid($req->param('uuid', NULL));
        $simulator_manager = new \Phposdck\SimulatorManager();
        $simulator_manager->setSimulator($simulator);
        $simulator = $simulator_manager->getSimulator();
        $res->json($simulator->toArray());
    } catch (\Exception $e)
    {
        $code = $e->getCode();
        if ($code)
        {
            $res->code($code);
        } else
        {
            $res->code(400);
        }

        $res->json([
            'message' => $e->getMessage()
        ]);
    }
});

$klein->respond('GET', '/simulators', function(Request $req, Response $res)
{
    $simulator_manager = new \Phposdck\SimulatorManager();

    try
    {
        $simulators = $simulator_manager->getAllSimulatorsContainers();
        $sims = [];
        foreach ($simulators as $simulator)
        {
            $sims[] = $simulator->toArray();
        }
        $res->json($sims);
    } catch (\Exception $e)
    {
        $code = $e->getCode();
        if ($code)
        {
            $res->code($code);
        } else
        {
            $res->code(400);
        }

        $res->json([
            'message' => $e->getMessage()
        ]);
    }
});

$klein->respond('POST', '/simulators', function(Request $req, Response $res, ServiceProvider $service)
{
    try
    {
        // get data
        $data = json_decode($req->body(), TRUE);

        // get tools
        $simulator = new \Phposdck\Simulator();
        $simulator_manager = new \Phposdck\SimulatorManager();

        // build simulator
        $simulator->setUuid(\Phposdck\Uuid::create());
        $simulator->setPort($simulator_manager->getNextSimulatorPort());
        $simulator->parseFromArray($data, TRUE);

        // check if interactive
        if (isset($data['IsInteractive']))
        {
            $simulator_manager->setIsInteractive($data['IsInteractive']);
        }

        // create the simulator
        $simulator_manager->setSimulator($simulator);
        $simulator_manager->createSimulatorContainer();
    } catch (\Exception $e)
    {
        $code = $e->getCode();
        if ($code)
        {
            $res->code($code);
        } else
        {
            $res->code(400);
        }

        $res->json([
            'message' => $e->getMessage()
        ]);
        return;
    }

    $res->json([
        'sim_uuid' => $simulator->getUuid(),
        'sim_port' => $simulator->getPort()
    ]);
});

$klein->respond('PUT', '/simulators/[:uuid]', function(Request $req, Response $res)
{
    try
    {
        // get uuid
        $uuid = $req->param('uuid', NULL);

        // get data
        $data = json_decode($req->body(), TRUE);

        // get tools
        $simulator = new \Phposdck\Simulator();
        $simulator_manager = new \Phposdck\SimulatorManager();

        // build simulator
        $simulator->setUuid($uuid);
        $simulator_manager->setSimulator($simulator);
        $simulator_manager->loadSimulator();

        $simulator->parseFromArray($data);

        // update the simulator
        $simulator_manager->updateSimulator();
    } catch (\Exception $e)
    {
        $code = $e->getCode();
        if ($code)
        {
            $res->code($code);
        } else
        {
            $res->code(400);
        }

        $res->json([
            'message' => $e->getMessage()
        ]);
        return;
    }

    $res->json([
        'sim_uuid' => $simulator->getUuid(),
        'sim_port' => $simulator->getPort()
    ]);
});

$klein->respond('DELETE', '/simulators/[:uuid]', function(Request $req, Response $res)
{
    try
    {
        $uuid = $req->param('uuid', NULL);
        $simulator = new \Phposdck\Simulator();
        $simulator->setUuid($uuid);
        $simulator_manager = new \Phposdck\SimulatorManager();
        $simulator_manager->setSimulator($simulator);
        $success = $simulator_manager->deleteSimulatorContainer();
        $res->json($success);
    } catch (\Exception $e)
    {
        $code = $e->getCode();
        if ($code)
        {
            $res->code($code);
        } else
        {
            $res->code(400);
        }

        $res->json([
            'message' => $e->getMessage()
        ]);
    }
});

$klein->respond('GET', '/getgrids', function (Request $req, Response $res)
{
    $res->json(\Phposdck\Utils::getGrids());
});

$klein->respond('GET', '/test', function (Request $req, Response $res)
{
    return '';
});

$klein->respond('GET', '/ping', function (Request $req, Response $res)
{
    return 'pong';
});

$klein->respond('GET', '/', function (Request $req, Response $res)
{
    $res->code(400);
    $res->json([
        'message' => 'Wrong path'
    ]);
});

$klein->dispatch();
