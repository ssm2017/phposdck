#!/bin/bash

# set text colors
ColRedF=$'\e[31m[Error] ';
ColGreenF=$'\e[32m[Success] ';
ColYellowF=$'\e[33m[Question] ';
INFO="[Info] "
ColReset=$'\e[0m';

check_if_root() {
	if [[ $EUID -ne 0 ]]; then
		echo -e "${ColRedF}This script must be run as root${ColReset}" 1>&2
		exit 1
	fi
}

get_opensimulator_folder() {
	OPENSIMULATOR_FOLDER="/srv/opensimulator"
	# check if path given
	if [ $# -lt 1 ];then
		echo "${INFO}No path given; Using default : ${OPENSIMULATOR_FOLDER}"
	else
		OPENSIMULATOR_FOLDER="${1}"
	fi

	yn=""
	while true;do
		echo -e "${ColYellowF}The opensimulator folder is set to ${OPENSIMULATOR_FOLDER}. Do you want to continue ?${ColReset}"
		read -p "${ColYellowF}(Yes= Y or y / No= N or n)${ColReset}" yn
		case $yn in
		  [Yy]* ) break;;
		  [Nn]* ) echo -e "${ColRedF}Installation cancelled.${ColReset}" && exit;;
			  * ) echo -e "${ColRedF}Please answer y or n.${ColReset}";;
		esac
	done

	echo "${INFO}The simulator folders will be created in ${OPENSIMULATOR_FOLDER}"
}

get_opensimulator_version() {
	OPENSIMULATOR_VERSION="0.9.0.0"
	# check if version given
	if [ $# -lt 1 ];then
		echo "${INFO}No OpenSimulator version given; Using default : ${OPENSIMULATOR_VERSION}"
	else
		OPENSIMULATOR_VERSION="${1}"
	fi

	yn=""
	while true;do
		echo -e "${ColYellowF}The OpenSimulator version is set to ${OPENSIMULATOR_VERSION}. Do you want to continue ?${ColReset}"
		read -p "${ColYellowF}(Yes= Y or y / No= N or n)${ColReset}" yn
		case $yn in
		  [Yy]* ) break;;
		  [Nn]* ) echo -e "${ColRedF}Installation cancelled.${ColReset}" && exit;;
			  * ) echo -e "${ColRedF}Please answer y or n.${ColReset}";;
		esac
	done

	echo "${INFO}The container image will be built using OpenSimulator version : ${OPENSIMULATOR_VERSION}"
}

create_folder() {
	if [ ! -d "${1}" ];then
		mkdir "${1}" 1>&2
		if [ $? -ne 0 ]; then
			echo -e "${ColRedF}Unable to create folder : ${1}.${ColReset}"
			echo -e "${ColRedF}Installation cancelled.${ColReset}"
			exit;
		fi
		echo -e "${ColGreenF}Folder : ${1} created.${ColReset}"
	else
		echo -e "${INFO}Folder : ${1} already exists."
	fi
}

copy_file() {
	# check if source file exists
	if [ ! -f "${1}" ];then
		echo -e "${ColRedF}File ${1} not found.${ColReset}"
		echo -e "${ColRedF}Installation cancelled.${ColReset}"
		exit;
	fi
	# check if destination file exists
	if [ ! -f "${2}" ];then
		cp "${1}" "${2}" 1>&2
		if [ $? -ne 0 ]; then
			echo -e "${ColRedF}Unable to copy file : ${1} to ${2}.${ColReset}"
			echo -e "${ColRedF}Installation cancelled.${ColReset}"
			exit;
		fi
		echo -e "${ColGreenF}File : ${1} copied to ${2}.${ColReset}"
	else
		echo -e "${INFO}File : ${2} already exists."
	fi
}