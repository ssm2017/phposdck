#!/bin/bash

get_script_path() {
	pushd `dirname $0` > /dev/null
	SCRIPTPATH=`pwd -P`
	popd > /dev/null
	cd "${SCRIPTPATH}"
}

# check if main script or not
if [ -z ${IS_MAIN+x} ]; then IS_MAIN=0; fi

if [ $IS_MAIN -eq 0 ];then
	get_script_path
	cd ..
	source includes/utils.sh
fi

ACTUAL_FOLDER=$(pwd)

if [ -z ${OPENSIMULATOR_FOLDER+x} ]; then get_opensimulator_folder ${1}; fi

get_opensimulator_version ${2}

# create a cache folder
echo -e "${INFO}Create a temp folder."
create_folder "${OPENSIMULATOR_FOLDER}/tarballs"

# create a temp workbench
echo -e "${INFO}Create a temp workbench."
if [ ! -d /tmp/phposdck/temp ];then
	mkdir /tmp/phposdck
else
	rm -R /tmp/phposdck && mkdir /tmp/phposdck
fi

echo -e "${INFO}Copy the Dockerfile."
copy_file ${ACTUAL_FOLDER}/config_files/Dockerfile /tmp/phposdck

cd "${OPENSIMULATOR_FOLDER}/tarballs"

# get the opensimulator archive if not exists
echo -e "${INFO}Get the OpenSimulator archive."
if [ ! -f "${OPENSIMULATOR_FOLDER}/tarballs/opensim-${OPENSIMULATOR_VERSION}.tar.gz" ];then
	# check if file exists
	curl -sSf http://dist.opensimulator.org/opensim-${OPENSIMULATOR_VERSION}.tar.gz > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		echo -e "${ColRedF}Error getting archive : http://dist.opensimulator.org/opensim-${OPENSIMULATOR_VERSION}.tar.gz${ColReset}"
		echo -e "${ColRedF}Installation cancelled.${ColReset}"
		exit;
	else
		wget http://dist.opensimulator.org/opensim-${OPENSIMULATOR_VERSION}.tar.gz
	fi
fi

cd /tmp/phposdck

# extract the archive
echo -e "${INFO}Extract the archive."
tar -zxf "${OPENSIMULATOR_FOLDER}/tarballs/opensim-${OPENSIMULATOR_VERSION}.tar.gz" --strip-components 1

# copy the config-include folder to defaults
echo -e "${INFO}Copy the config-include folder."
cp -R /tmp/phposdck/bin/config-include "${OPENSIMULATOR_FOLDER}/default/"

# make another archive containing only bin folder
echo -e "${INFO}Create a new archive."
tar -zcf opensim.tar.gz bin

# build the docker image
echo -e "${INFO}Build the docker image."
docker build -t opensimulator .

# delete temp workbench
echo -e "${INFO}Delete the temp workbench."
rm -R /tmp/phposdck/temp
