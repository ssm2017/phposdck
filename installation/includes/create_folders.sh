#!/bin/bash

get_script_path() {
	pushd `dirname $0` > /dev/null
	SCRIPTPATH=`pwd -P`
	popd > /dev/null
	cd "${SCRIPTPATH}"
}

# check if main script or not
if [ -z ${IS_MAIN+x} ]; then IS_MAIN=0; fi

if [ $IS_MAIN -eq 0 ];then
	get_script_path
	cd ..
	source includes/utils.sh
fi

get_opensimulator_folder ${1}

ACTUAL_FOLDER=$(pwd)

create_folder "${OPENSIMULATOR_FOLDER}"
create_folder "${OPENSIMULATOR_FOLDER}/default"
create_folder "${OPENSIMULATOR_FOLDER}/default/archives"
create_folder "${OPENSIMULATOR_FOLDER}/default/config"
create_folder "${OPENSIMULATOR_FOLDER}/default/log"
create_folder "${OPENSIMULATOR_FOLDER}/sims"

copy_file ${ACTUAL_FOLDER}/config_files/OpenSim.exe.config "${OPENSIMULATOR_FOLDER}/default/OpenSim.exe.config"