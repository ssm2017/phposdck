#!/bin/bash

# get and run sherpa
docker run -d \
--name sherpa \
--userns=host \
-e CONFIG='[
    { 
        "Path" : "/",
        "Access": "allow",
        "Addresses": ["10.0.0.0/8", "192.168.0.0/16", "172.0.0.0/8"]
    }
]' \
-v /var/run/docker.sock:/tmp/docker.sock \
-p 4550:4550 \
djenriquez/sherpa --allow

# get and run the web server
docker-compose up -d

# get composer
docker pull composer/composer

cd /srv/www/phposdck
docker run --rm --interactive --tty \
--volume $PWD:/app \
--user $(id -u):$(id -g) \
composer/composer install
