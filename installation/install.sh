#!/bin/bash

OPENSIMULATOR_FOLDER="/srv/opensimulator"
IS_MAIN=1

# get the script location (for inclusions)
get_script_path() {
	pushd `dirname $0` > /dev/null
	SCRIPTPATH=`pwd -P`
	popd > /dev/null
	cd "${SCRIPTPATH}"
}
if [ -z ${SCRIPTPATH+x} ]; then get_script_path; fi

source ${SCRIPTPATH}/includes/utils.sh

# check if root
check_if_root

# create the simulators folders
source ${SCRIPTPATH}/includes/create_folders.sh

# create the Docker image
source ${SCRIPTPATH}/includes/create_image.sh

echo -e "${ColGreenF}Folder : ${1} created.${ColReset}"