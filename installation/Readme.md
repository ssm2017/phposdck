# PhpOsDck Installation

[![N|OpenSimulator](http://opensimulator.org/images/d/d4/Os_b_200x50_r.png)](http://opensimulator.org)

Here are the steps to install phposdck.

  - Configure users
  - Create Default folders and the Docker image
  - Install web server

## Configure users

**Use these settings with care because it can break an already configured Docker server**

To be able to share volumes between host and containers, we use the Docker's user namespace (The OpenSimulator image is using a user/group called "opensim" and using id 1000 in the container and id 501000 on the host) [Source](https://resourcepool.io/fr/2016/09/09/francais-le-user-namespace-dans-docker/). 

### Create users on the host
("dockremap" user will be the "root" user in containers and "dockremap-user" will be the first created user (1000) in containers).

```sh
groupadd -g 500000 dockremap && 
groupadd -g 501000 dockremap-user && 
useradd -u 500000 -g dockremap -s /bin/false dockremap && 
useradd -u 501000 -g dockremap-user -s /bin/false dockremap-user
```
```sh
echo "dockremap:500000:65536" >> /etc/subuid && 
echo "dockremap:500000:65536" >>/etc/subgid
```
### Configure Docker

In the Docker config file, add :

```json
{
  "userns-remap": "default"
}
```

Restart Docker
```sh
systemctl daemon-reload && systemctl restart docker
```
## Create Default folders and the Docker image
You can use the "install.sh" script (as root) to create the default folders and the Docker image.

Here are the explanations of the process :

### Create default folders
PhpOsDck needs to have a default folder structure to mount config files in containers.

By default, PhpOsDck is using the folder **/srv/opensimulator**.

Here is the basic default structure :
```
├── default (Template folder that will be cloned to every new simulator)
│   ├── archives (Archives files (IAR/OAR))
│   ├── config (Simulator ini files)
│   ├── config-include (Well known folder)
│   │   ├── CenomeCache.ini.example
│   │   ├── FlotsamCache.ini
│   │   ├── FlotsamCache.ini.example
│   │   ├── GridCommon.ini.example
│   │   ├── GridHypergrid.ini
│   │   ├── Grid.ini
│   │   ├── HyperSimianGrid.ini
│   │   ├── osslEnable.ini
│   │   ├── SimianGrid.ini
│   │   ├── StandaloneCommon.ini
│   │   ├── StandaloneCommon.ini.example
│   │   ├── StandaloneHypergrid.ini
│   │   ├── Standalone.ini
│   │   └── storage
│   │       └── SQLiteStandalone.ini
│   ├── log (Simulator log files (The path is set in OpenSim.exe.config)
│   └── OpenSim.exe.config (Custom config for log files)
└── sims (This folder will contain all the created OpenSimulator containers volumes using the sim uuid as folder name)
```

If using the Docker user namespaces, the php Docker container will use user id 500033 so the folders will have these ids.

To be able to share the folders with the opensim user, the "opensim" user in the container will be member of the group www-data
but to be able to share folders with the php script, we can do this :
  - create a user for the id 500033 : ```useradd -u 500033 -g docker-www-data -s /bin/false docker-www-data```
  - create a group for the id 500033 : ```groupadd -g 500033 docker-www-data```
  - add the user docker-www-data to the "dockremap-user" group : ```addgroup docker-www-data dockremap-user```
  - change the ownership of these folders to "dockremap-user" : ```chown -R dockremap-user:dockremap-user /srv/opensimulator```
  - change the perms of the simulator folder :
```
find /srv/opensimulator | while read file; do [[ -f "$file" ]] && chmod 664 "$file"; [[ -d "$file" ]] && chmod 2775 "$file"; done
```

### Create the Docker image
The Docker image will be created using the "Dockerfile" contained in the installation folder.

You will need to have an opensim.tar.gz archive containing the OpenSimulator bin folder only.

## Install web server
You can use your own web server or you can use the "installation/webserver/install.sh" file (using the docker user instead of root).

The installation script considers that PhpOsDck is located in **/srv/www/phposdck**

Requirements :
  - php >= 7
  - composer
  - docker-compose

Here are the explanations of the process :

### Pull sherpa image
The djenriquez/sherpa image is used to run a Docker web api with the ability to restrain access to the api.

The image will listen on port 4550 and will be restricted to local ips only.

You will need to inform the php script about the shepar's ip:port in the /srv/www/phposdck/config/config.ini file.

### Create and run a php web server
The script is running "docker-compose up -d" to create and run 2 Docker images (one for nginx and one for php-fpm).

The "docker-compose.yml" file will consider that the public web server script is located in /srv/www/phposdck/public/index.php and that the OpenSimulator sims folder is located in /srv/opensimulator/sims.

This file will also take the vhost (for nginx) in /srv/www/phposdck/installation/webserver/conf.d

If you would like to use other folders, change the content of "docker-compose.yml" and "conf.d/phposdck.conf".

Here is the nginx url rewrite rule (translate it to apache if needed) :
```
location / {
        try_files $uri @rewrite;
    }

    location @rewrite {
        rewrite ^/(.*)$ /index.php?q=$1;
    }
```

### Pull composer image
The composer image will be used temporarily to install the dependencies.