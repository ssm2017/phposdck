<?php

namespace Phposdck;

class SimulatorManager
{

    /**
     * Simulator object
     * @var type \Phposdck\Simulator
     */
    private $Simulator;

    /**
     * Simulator container object
     * @var type \Docker\API\Model\Container
     */
    private $SimulatorContainer;

    /**
     * Docker client object
     * @var type \Docker\Docker
     */
    private $DockerClient;

    /**
     * Docker container is interactive
     * @var type bool
     */
    private $IsInteractive = FALSE;

    public function getIsInteractive()
    {
        return $this->IsInteractive;
    }

    public function setIsInteractive($IsInteractive = FALSE)
    {
        $this->IsInteractive = $IsInteractive;
    }

    private function getDockerClient()
    {
        if ($this->DockerClient === NULL)
        {
            $this->setDockerClient();
        }
        return $this->DockerClient;
    }

    private function setDockerClient()
    {
        $client = new \Docker\DockerClient([
            'remote_socket' => $GLOBALS['config']['Docker']['socket'],
            'ssl' => $GLOBALS['config']['Docker']['use_ssl'],
        ]);
        $this->DockerClient = new \Docker\Docker($client);
    }

    public function loadSimulator()
    {
        if ($this->SimulatorContainer === NULL)
        {
            // get the container
            $this->getSimulatorContainer();
        }

        // get values
        $config = $this->SimulatorContainer->getConfig();
        $labels = $config->getLabels();

        // owner
        $this->Simulator->setOwnerName($labels['simulator.owner.name']);
        $OwnerUuid = $labels['simulator.owner.uuid'];
        $this->Simulator->setOwnerUuid($OwnerUuid);

        // hypergrid vs standalone
        $this->Simulator->setIsHypergrid(($labels['simulator.is.hypergrid'] == 'true') ? TRUE : FALSE);
        $IsStandalone = ($labels['simulator.is.standalone'] == 'true') ? TRUE : FALSE;
        if (!$IsStandalone)
        {
            $this->Simulator->setGridName($labels['simulator.grid.name']);
        }

        // port
        $label_port = $labels['simulator.port'];
        $first_exposed_port = rtrim(key($config->getExposedPorts()), '/tcp');
        if ($label_port != $first_exposed_port)
        {
            throw new \Exception('Wrong port assignment for simulator : ' . $uuid);
        }
        $this->Simulator->setPort($label_port);

        // inis
        $sim_path = $GLOBALS['config']['Base']['host_folder'] . '/sims/' . $OwnerUuid . '/sims/' . $this->Simulator->getUuid();
        $IniFiles = [
            'OpenSim.ini' => \Phposdck\Utils::readIni($sim_path . '/OpenSim.ini'),
            'Database.ini' => \Phposdck\Utils::readIni($sim_path . '/config/Database.ini')
        ];
        if (is_file($sim_path . '/config/Custom.ini')) {
            $IniFiles['Custom.ini'] = \Phposdck\Utils::readIni($sim_path . '/config/Custom.ini');
        }
        if ($IsStandalone)
        {
            $IniFiles['StandaloneCommon.ini'] = \Phposdck\Utils::readIni($sim_path . '/config-include/StandaloneCommon.ini');
        } else
        {
            $IniFiles['GridCommon.ini'] = \Phposdck\Utils::readIni($sim_path . '/config-include/GridCommon.ini');
        }
        $this->Simulator->setIniFiles($IniFiles);
    }

    public function getSimulator()
    {
        if ($this->SimulatorContainer === NULL)
        {
            // get the container
            $this->loadSimulator();
        }
        return $this->Simulator;
    }

    public function setSimulator($simulator)
    {
        $this->Simulator = $simulator;
    }

    public function getSimulatorContainer()
    {
        $docker = $this->getDockerClient();
        try
        {
            $this->SimulatorContainer = $docker->getContainerManager()->find($this->Simulator->getUuid());
        } catch (\Exception $e)
        {
            throw $e;
        }
        return $this->SimulatorContainer;
    }

    public function getAllSimulatorsContainers()
    {
        $simulators = [];

        // get the docker client
        $docker = $this->getDockerClient();
        try
        {
            // get all containers
            $containers = $docker->getContainerManager()->findAll(['all' => TRUE]);
            foreach ($containers as $container)
            {
                // get the opensimulator ones
                if ($container->getImage() == $GLOBALS['config']['Docker']['image'])
                {
                    // get the name (uuid)
                    $name = trim($container->getNames()[0], '/');
                    // create the simulator
                    $simulator = new \Phposdck\Simulator();
                    $simulator->setUuid($name);
                    $this->setSimulator($simulator);

                    try
                    {
                        $this->getSimulatorContainer();
                        $this->loadSimulator();
                        $simulators[] = $this->getSimulator();
                    } catch (\Exception $e)
                    {
                        throw $e;
                    }
                }
            }
        } catch (\Exception $e)
        {
            throw $e;
        }
        return $simulators;
    }

    public function createSimulatorContainer()
    {
        // get some repetitive sim infos
        // uuids
        $sim_uuid = $this->Simulator->getUuid();
        $sim_owner_uuid = $this->Simulator->getOwnerUuid();
        // folders
        $sim_owner_folder = $GLOBALS['config']['Base']['host_folder'] . '/sims/' . $sim_owner_uuid;
        $sim_folder = $sim_owner_folder . '/sims/' . $sim_uuid;
        $container_folder = $GLOBALS['config']['Base']['container_folder'];

        // standalone vs grid
        $is_standalone = is_null($this->Simulator->getGridName()) ? TRUE : FALSE;

        // port
        $sim_port = $this->Simulator->getPort();
        if (is_null($sim_port))
        {
            $sim_port = $this->getNextSimulatorPort();
            $this->Simulator->setPort($port);
        }

        // check if owner already has a folder
        if (!is_dir($sim_owner_folder))
        {
            if (!mkdir($sim_owner_folder))
            {
                throw new \Exception('Unable to create owner folder', 500);
            }
            if (!mkdir($sim_owner_folder . '/sims'))
            {
                throw new \Exception('Unable to create sim owner folder', 500);
            }
        }

        // create the archives folder
        if (!is_dir($sim_owner_folder . '/archives'))
        {
            if (!mkdir($sim_owner_folder . '/archives'))
            {
                throw new \Exception('Unable to create owner archives folder', 500);
            }
        }

        // create the sim folder
        if (!is_dir($sim_folder))
        {
            if (!mkdir($sim_folder))
            {
                throw new \Exception('Unable to create sim folder', 500);
            }
        }

        // create the log folder
        if (!is_dir($sim_folder . '/log'))
        {
            if (!mkdir($sim_folder . '/log'))
            {
                throw new \Exception('Unable to create sim log folder', 500);
            }
        }

        // create the config folder
        if (!is_dir($sim_folder . '/config'))
        {
            if (!mkdir($sim_folder . '/config'))
            {
                throw new \Exception('Unable to create sim config folder', 500);
            }
        }

        // create the config-include folder
        if (!is_dir($sim_folder . '/config-include'))
        {
            if (!mkdir($sim_folder . '/config-include'))
            {
                throw new \Exception('Unable to create sim config-include folder', 500);
            }
        }

        // write inis files
        $this->writeInis();

        // create the OpenSim.exe.config file
        if (!copy('../config/simulator/OpenSim.exe.config', $sim_folder . '/OpenSim.exe.config'))
        {
            throw new \Exception('Unable to copy OpenSim.exe.config file', 500);
        }

        // check if valid
        try
        {
            $this->Simulator->isValid();
        } catch (\Exception $e)
        {
            throw new \Exception('Simulator not valid : ' . $e->getMessage(), 400);
        }

        // create the container
        // basic config
        $containerConfig = new \Docker\API\Model\ContainerConfig();
        $hostConfig = new \Docker\API\Model\HostConfig();

        $containerConfig->setImage($GLOBALS['config']['Docker']['image']);

        // volumes
        $volumes = [
            $sim_owner_folder . '/archives:' . $container_folder . '/bin/archives',
            $sim_folder . '/log:' . $container_folder . '/bin/log',
            $sim_folder . '/config:' . $container_folder . '/bin/config',
            $sim_folder . '/OpenSim.exe.config:' . $container_folder . '/bin/OpenSim.exe.config',
            $sim_folder . '/OpenSim.ini:' . $container_folder . '/bin/OpenSim.ini'
        ];
        if ($is_standalone)
        {
            $volumes[] = $sim_folder . '/config-include/StandaloneCommon.ini:' . $container_folder . '/bin/config-include/StandaloneCommon.ini';
        } else
        {
            $volumes[] = $sim_folder . '/config-include/GridCommon.ini:' . $container_folder . '/bin/config-include/GridCommon.ini';
        }
        $hostConfig->setBinds($volumes);

        // ports
        $exposed_ports = [];
        $port_map = new \ArrayObject();

        // sim port
        $port = (string) $sim_port;
        $exposed_ports[$port . '/tcp'] = (object) [];
        $port_binding = new \Docker\API\Model\PortBinding();
        $port_binding->setHostPort($port);
        $port_binding->setHostIp('0.0.0.0');
        $port_map[$port . '/tcp'] = [$port_binding];

        // radmin port
        $port = (string) ($sim_port + 1);
        $exposed_ports[$port . '/tcp'] = (object) [];
        $port_binding = new \Docker\API\Model\PortBinding();
        $port_binding->setHostPort($port);
        $port_binding->setHostIp('0.0.0.0');
        $port_map[$port . '/tcp'] = [$port_binding];

        // regions ports
        for ($i = ($sim_port + 10); $i < ($sim_port + 100); $i++)
        {
            $port = (string) $i;
            $exposed_ports[$port . '/udp'] = (object) [];
            $port_binding = new \Docker\API\Model\PortBinding();
            $port_binding->setHostPort($port);
            $port_binding->setHostIp('0.0.0.0');
            $port_map[$port . '/udp'] = [$port_binding];
        }

        // expose ports
        $containerConfig->setExposedPorts($exposed_ports);
        $hostConfig->setPortBindings($port_map);

        // add labels
        $labels = [
            'simulator.owner.name' => $this->Simulator->getOwnerName(),
            'simulator.owner.uuid' => $this->Simulator->getOwnerUuid(),
            'simulator.port' => (string) $sim_port,
            'simulator.is.standalone' => $is_standalone ? 'true' : 'false',
            'simulator.is.hypergrid' => $this->Simulator->getIsHypergrid() ? 'true' : 'false'
        ];
        if (!$is_standalone)
        {
            $labels['simulator.grid.name'] = $this->Simulator->getGridName();
        }
        $containerConfig->setLabels($labels);

        // set interactive
        if ($this->IsInteractive)
        {
            //$containerConfig->setAttachStdin(true);
            $containerConfig->setOpenStdin(true);
            $containerConfig->setTty(true);
        }

        // set some env
        $containerConfig->setEnv(['MONO_THREADS_PER_CPU=' . $GLOBALS['config']['Base']['mono_threads_per_cpu']]);

        // set config
        $containerConfig->setHostConfig($hostConfig);

        // get docker client
        $docker = $this->getDockerClient();
        $containerManager = $docker->getContainerManager();

        // create the container
        try
        {
            $containerManager->create($containerConfig, ['name' => $sim_uuid]);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function updateSimulator()
    {
        // write inis files
        $this->writeInis();
    }

    public function deleteSimulatorContainer()
    {
        $messages = [];

        $uuid = $this->Simulator->getUuid();
        // get docker client
        $docker = $this->getDockerClient();
        // get the container
        $container = $this->getSimulatorContainer();
        // unpause the simulator if paused
        if ($this->SimulatorContainer->getState()->getPaused())
        {
            try
            {
                $docker->getContainerManager()->unpause($uuid);
            } catch (\Exception $e)
            {
                $messages[] = $e->getMessage();
            }
        }

        // remove the container
        try
        {
            $docker->getContainerManager()->remove($uuid, ['force' => TRUE]);
            $messages[] = 'Container deleted';
        } catch (\Exception $e)
        {
            $messages[] = $e->getMessage();
        }

        // remove the simulator folder
        $sim_owner_folder = $GLOBALS['config']['Base']['host_folder'] . '/sims/' . $this->SimulatorContainer->getConfig()->getLabels()['simulator.owner.uuid'];

        if (is_dir($sim_owner_folder . '/' . $uuid))
        {
            exec(
                    'rm -r ' .
                    $sim_owner_folder . '/' . $uuid .
                    ' 2>&1', $last_response, $last_code
            );
            if ($last_code != 0)
            {
                $messages[] = $last_response[0];
            }
            $messages[] = 'Sim folder deleted';
        }

        // check if the simulator owner folder is empty
        $scanned_directory = array_diff(scandir($sim_owner_folder), array('..', '.'));
        if (!count($scanned_directory))
        {
            if (!\Phposdck\Utils::delTree($sim_owner_folder))
            {
                $messages[] = 'Unable to delete sim owner folder';
            } else
            {
                $messages[] = 'Owner folder deleted';
            }
        }

        return $messages;
    }

    public function startSimulatorContainer()
    {
        $uuid = $this->Simulator->getUuid();
        $docker = $this->getDockerClient();
        try
        {
            return $docker->getContainerManager()->start($uuid);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function stopSimulatorContainer()
    {
        $uuid = $this->Simulator->getUuid();
        $docker = $this->getDockerClient();
        try
        {
            return $docker->getContainerManager()->stop($uuid);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function restartSimulatorContainer()
    {
        $uuid = $this->Simulator->getUuid();
        $docker = $this->getDockerClient();
        try
        {
            return $docker->getContainerManager()->restart($uuid);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function killSimulatorContainer()
    {
        $uuid = $this->Simulator->getUuid();
        $docker = $this->getDockerClient();
        try
        {
            return $docker->getContainerManager()->kill($uuid);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function getSimulatorContainerLogs($config = [])
    {
        $uuid = $this->Simulator->getUuid();
        $docker = $this->getDockerClient();
        try
        {
            return $docker->getContainerManager()->logs($uuid, $config);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function purgeSimulators()
    {
        $messages = [];

        // purge by containers
        $simulators = $this->getAllSimulatorsContainers();
        if (count($simulators))
        {
            $messages[] = "purge by containers";
            foreach ($simulators as $simulator)
            {
                $this->setSimulator($simulator);
                $uuid = $simulator->getUuid();

                // check if there is a folder
                $has_folder = FALSE;
                try
                {
                    $sim_owner_folder = $GLOBALS['config']['Base']['host_folder'] . '/sims/' . $simulator->getOwnerUuid();
                    if (is_dir($sim_owner_folder . '/' . $uuid))
                    {
                        $has_folder = TRUE;
                    }
                } catch (\Exception $e)
                {
                    $messages[] = $e->getMessage();
                }

                // check if there is a port
                $has_port = FALSE;
                try
                {
                    $simulator->getPort();
                    $has_port = TRUE;
                } catch (\Exception $e)
                {
                    $messages[] = $e->getMessage();
                }
                $messages[] = [
                    'container' => $uuid,
                    'has folder' => $has_folder,
                    'has port' => $has_port
                ];
                if (!$has_folder || !$has_port)
                {
                    $messages[] = 'Delete container : ' . $uuid;
                    $this->setSimulator($simulator);
                    $messages[] = $this->deleteSimulatorContainer();
                }
            }
        }

        // purge by folders
        $owners_folders = array_diff(scandir($GLOBALS['config']['Base']['host_folder'] . '/sims/'), array('..', '.'));

        // check every folder
        if (count($owners_folders))
        {
            foreach ($owners_folders as $owner_folder)
            {
                $owner_folder_path = $GLOBALS['config']['Base']['host_folder'] . '/sims/' . $owner_folder;
                $sims_folders = array_diff(scandir($owner_folder_path), array('..', '.', 'archives'));
                if (count($sims_folders))
                {
                    $messages[] = "purge by folders";
                    foreach ($sims_folders as $uuid)
                    {
                        if (\Phposdck\Uuid::check($uuid))
                        {
                            // create a simulator
                            $simulator = new \Phposdck\Simulator();
                            $simulator->setUuid($uuid);
                            $sim_folder_path = $owner_folder_path . '/' . $uuid;
                            $this->setSimulator($simulator);

                            // check if there is a container
                            $this->SimulatorContainer = NULL;
                            try
                            {
                                $this->getSimulator();
                                $messages[] = [
                                    'container' => $uuid,
                                    'has container' => TRUE
                                ];
                            } catch (\Exception $e)
                            {
                                // delete the folder
                                $messages[] = $e->getMessage();
                                $messages[] = 'Delete sim folder : ' . $uuid;
                                if (is_dir($sim_folder_path))
                                {
                                    exec(
                                            'rm -r ' .
                                            $sim_folder_path .
                                            ' 2>&1', $last_response, $last_code
                                    );
                                    if ($last_code != 0)
                                    {
                                        $messages[] = $last_response[0];
                                    }
                                    // check if the simulator owner folder is empty
                                    $scanned_directory = array_diff(scandir($owner_folder_path), array('..', '.'));
                                    if (!count($scanned_directory))
                                    {
                                        if (!rmdir($owner_folder_path))
                                        {
                                            $messages[] = 'Unable to delete sim owner folder : ' . $owner_folder;
                                        } else
                                        {
                                            $messages[] = 'Owner folder deleted : ' . $owner_folder;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else
                {
                    if (!\Phposdck\Utils::delTree($owner_folder_path))
                    {
                        $messages[] = 'Unable to delete sim owner folder : ' . $owner_folder;
                    } else
                    {
                        $messages[] = 'Owner folder deleted : ' . $owner_folder;
                    }
                }
            }
        }
        return $messages;
    }

    public function getNextSimulatorPort()
    {
        // get all simulator ports
        $sims = $this->getAllSimulatorsContainers();

        // if no container, take the first port from config
        if (!count($sims))
        {
            return $GLOBALS['config']['Ports']['first_port'];
        }

        // gather all ports in an array
        $ports = [];
        foreach ($sims as $sim)
        {
            $ports[] = $sim->getPort();
        }
        sort($ports);

        // browse the array to get the next port
        $port = 0;
        $next_port = $GLOBALS['config']['Ports']['first_port'];
        while ($next_port < $GLOBALS['config']['Ports']['last_port'])
        {
            if (!in_array($next_port, $ports))
            {
                $port = $next_port;
                break;
            }
            $next_port += $GLOBALS['config']['Ports']['port_increment'];
        }
        return $port;
    }

    public function writeInis()
    {
        // get the sim folder
        $sim_folder = $GLOBALS['config']['Base']['host_folder'] . '/sims/' . $this->Simulator->getOwnerUuid() . '/sims/' . $this->Simulator->getUuid();
        // standalone vs grid
        $is_standalone = is_null($this->Simulator->getGridName()) ? TRUE : FALSE;

        // create the OpenSim.ini file
        $ini_files = $this->Simulator->getIniFiles();

        $content = $ini_files['OpenSim.ini'];
        try
        {
            \Phposdck\Utils::writeIni($sim_folder . '/OpenSim.ini', $content);
        } catch (\Exception $e)
        {
            throw new \Exception('Error copying OpenSim.ini file : ' . $e->getMessage());
        }

        // standalone vs grid
        if ($is_standalone)
        {
            $content = $ini_files['StandaloneCommon.ini'];
            try
            {
                \Phposdck\Utils::writeIni($sim_folder . '/config-include/StandaloneCommon.ini', $content);
            } catch (\Exception $e)
            {
                throw new \Exception('Error copying StandaloneCommon.ini file : ' . $e->getMessage());
            }
        } else
        {
            $content = $ini_files['GridCommon.ini'];
            try
            {
                \Phposdck\Utils::writeIni($sim_folder . '/config-include/GridCommon.ini', $content);
            } catch (\Exception $e)
            {
                throw new \Exception('Error copying GridCommon.ini file : ' . $e->getMessage());
            }
        }

        // create the Database.ini file
        $content = [
            // manage database (only sqlite actually)
            'DatabaseService' => [
                'Include-Storage' => 'config-include/storage/SQLiteStandalone.ini'
            ]
        ];
        try
        {
            \Phposdck\Utils::writeIni($sim_folder . '/config/Database.ini', $content);
        } catch (\Exception $e)
        {
            throw new \Exception('Error copying Database.ini file : ' . $e->getMessage());
        }

        // create the Custom.ini file
        if (isset($ini_files['Custom.ini']) && count($ini_files['Custom.ini']))
        {
            $content = $ini_files['Custom.ini'];
            try
            {
                \Phposdck\Utils::writeIni($sim_folder . '/config/Custom.ini', $content);
            } catch (\Exception $e)
            {
                throw new \Exception('Error copying Custom.ini file : ' . $e->getMessage());
            }
        }
    }

}
