<?php

namespace Phposdck;

class Utils
{

    static public function getConfig()
    {
        $config_file = '../config/config.ini';
        // check if file exists
        if (!file_exists($config_file))
        {
            throw new \Exception('Config file not found', 500);
        }
        try
        {
            $GLOBALS['config'] = self::readIniFromFile($config_file);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    static public function writeIni($ini_file, $content)
    {
        $writer = new \Piwik\Ini\IniWriter();
        try
        {
            $writer->writeToFile($ini_file, $content);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    static public function readIni($path)
    {
        $content = "";
        $from_web = FALSE;
        if (substr($path, 0, 4) == 'http')
        {
            return self::readIniFromWeb($path);
        } else
        {
            return self::readIniFromFile($path);
        }
    }

    static public function readIniFromFile($path)
    {
        $reader = new \Piwik\Ini\IniReader();
        $reader->setUseNativeFunction(FALSE);
        try
        {
            return $reader->readFile($path);
        } catch (\Exception $e)
        {
            throw $e;
        }
    }

    static public function readIniFromWeb($url)
    {
        $context = stream_context_create([
            'http' => [
                'user_agent' => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0"
            ]
        ]);
        $file_content = file_get_contents($url, FALSE, $context);
        if ($file_content === FALSE)
        {
            throw new \Exception('Error getting file content : ' . $url);
        } else
        {
            $reader = new \Piwik\Ini\IniReader();
            $reader->setUseNativeFunction(FALSE);
            try
            {
                return $reader->readString($file_content);
            } catch (\Exception $e)
            {
                throw $e;
            }
        }
    }

    // http://php.net/manual/en/function.rmdir.php#110489
    public static function delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file)
        {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public static function getGrids() {
        $available_grids = [];
        // get files from config
        $grids = array_diff(scandir('../config/simulator/grids'), ['..', '.', 'standalone.ini']);
        if (count($grids)) {
            foreach($grids as $grid) {
                $available_grids[] = substr($grid, 0, -4);
            }
        }
        return $available_grids;
    }
}
