<?php

namespace Phposdck;

class Simulator
{

    /**
     * The uuid of the simulator (also the container name)
     * @var type \Phposdck\Uuid
     */
    private $Uuid;

    /**
     * The starting port of the simulator
     * @var type int
     */
    private $Port;

    /**
     * The owner name of the simulator
     * @var type string
     */
    private $OwnerName;

    /**
     * The uuid of the owner name
     * @var type \Phposdck\Uuid
     */
    private $OwnerUuid;

    /**
     * True if the simulator is a hypergrid enable
     * @var type bool
     */
    private $IsHypergrid;

    /**
     * The grid name if not standalone
     * @var type string
     */
    private $GridName;

    /**
     * Array containing ini files as objects
     * @var type array
     */
    private $IniFiles = [];

    // getters
    public function getUuid()
    {
        if (is_null($this->Uuid))
        {
            $this->Uuid = \Phposdck\Uuid::create();
        }
        return $this->Uuid;
    }

    public function getPort()
    {
        return $this->Port;
    }

    public function getOwnerName()
    {
        return $this->OwnerName;
    }

    public function getOwnerUuid()
    {
        return $this->OwnerUuid;
    }

    public function getIsHypergrid()
    {
        return $this->IsHypergrid;
    }

    public function getGridName()
    {
        return $this->GridName;
    }

    public function getIniFiles()
    {
        return $this->IniFiles;
    }

    // setters
    public function setUuid($Uuid)
    {
        if (is_null($Uuid) || $Uuid == \Phposdck\Uuid::UUID_ZERO || !\Phposdck\Uuid::check($Uuid))
        {
            throw new \Exception('Wrong simulator uuid');
        } else
        {
            $this->Uuid = $Uuid;
        }
    }

    public function setPort($Port)
    {
        $this->Port = $Port;
    }

    public function setOwnerName($OwnerName)
    {
        $this->OwnerName = $OwnerName;
    }

    public function setOwnerUuid($OwnerUuid)
    {
        if (is_null($OwnerUuid) || $OwnerUuid == \Phposdck\Uuid::UUID_ZERO || !\Phposdck\Uuid::check($OwnerUuid))
        {
            throw new \Exception('Wrong owner uuid');
        } else
        {
            $this->OwnerUuid = $OwnerUuid;
        }
    }

    public function setIsHypergrid($IsHypergrid = TRUE)
    {
        $this->IsHypergrid = $IsHypergrid;
    }

    public function setGridName($GridName)
    {
        // check if the grid exist in config files
        if ($GridName != 'default')
        {
            if (!in_array($GridName, \Phposdck\Utils::getGrids()))
            {
                throw new \Exception('Wrong grid name');
            }
        }
        $this->GridName = $GridName;
    }

    public function setIniFiles($IniFiles = [])
    {
        $this->IniFiles = $IniFiles;
    }

    // utils
    public function parseFromArray($content, $new = TRUE)
    {
        if ($new)
        {
            // set owner name
            $this->OwnerName = $content['OwnerName'];
            // set owner uuid
            $this->OwnerUuid = $content['OwnerUuid'];
            // set the grid (if any)
            if (isset($content['GridName']))
            {
                if (!empty($content['GridName']))
                {
                    $this->setGridName($content['GridName']);
                }
            }
            $is_standalone = is_null($this->GridName) ? TRUE : FALSE;
            // set if hypergrid
            $this->IsHypergrid = FALSE;
            if (isset($content['IsHypergrid']))
            {
                $this->IsHypergrid = $content['IsHypergrid'];
            }
        }

        // set inis
        $inis = [];

        // set OpenSim.ini
        $path = '../config/simulator/OpenSim.ini';
        if (isset($content['DefaultOpenSimIni']))
        {
            $path = $content['DefaultOpenSimIni'];
        }
        $data = \Phposdck\Utils::readIni($path);
        // fill values from submitted
        foreach ($content['OpenSim.ini'] as $k => $v)
        {
            $data[$k] = $v;
        }
        // override mandatory values
        $data['Const']['BaseHostname'] = $GLOBALS['config']['Base']['host_name'];
        $data['Const']['PublicPort'] = $this->Port;
        $data['Const']['PrivatePort'] = $this->Port;
        $data['Network']['http_listener_port'] = $this->Port;
        $data['Startup']['PIDFile'] = './log/' . $this->Uuid . '.pid';
        $data['Startup']['allow_regionless'] = 'true';
        $data['Startup']['region_info_source'] = 'web';
        $params = '?InternalPort=' . (string) ($this->Port + 10) . '&ExternalHostName=' . base64_encode($GLOBALS['config']['Base']['host_name']);
        $data['Startup']['regionload_webserver_url'] = $content['OpenSim.ini']['Startup']['regionload_webserver_url'] . $params;
        $data['RemoteAdmin']['enabled'] = 'true';
        $data['RemoteAdmin']['port'] = $this->Port + 1;
        // manage architecture
        if (isset($content['OpenSim.ini']['Architecture']['Include-Architecture']))
        {
            $data['Architecture']['Include-Architecture'] = $content['OpenSim.ini']['Architecture']['Include-Architecture'];
        } else
        {
            if ($is_standalone)
            {
                if ($this->IsHypergrid)
                {
                    $data['Architecture']['Include-Architecture'] = "config-include/StandaloneHypergrid.ini";
                } else
                {
                    $data['Architecture']['Include-Architecture'] = "config-include/Standalone.ini";
                }
            } else
            {
                if ($this->IsHypergrid)
                {
                    $data['Architecture']['Include-Architecture'] = "config-include/GridHypergrid.ini";
                } else
                {
                    $data['Architecture']['Include-Architecture'] = "config-include/Grid.ini";
                }
            }
        }
        $inis['OpenSim.ini'] = $data;

        if ($is_standalone)
        {
            // set StandaloneCommon.ini
            $path = '../config/simulator/grids/standalone.ini';
            if (isset($content['DefaultStandaloneCommonIni']))
            {
                $path = $content['DefaultStandaloneCommonIni'];
            }
            $data = \Phposdck\Utils::readIni($path);
            // fill values from submitted
            if (isset($content['StandaloneCommon.ini']))
            {
                foreach ($content['StandaloneCommon.ini'] as $k => $v)
                {
                    $data[$k] = $v;
                }
            }
            // override mandatory values
            $data['Hypergrid']['HomeURI'] = '${Const|BaseURL}:${Const|PublicPort}';
            $data['Hypergrid']['GatekeeperURI'] = '${Const|BaseURL}:${Const|PublicPort}';
            // remove database params
            unset($data['DatabaseService']);
            $inis['StandaloneCommon.ini'] = $data;
        } else
        {
            // get grid name
            $grid_name = $this->GridName;
            // set GridCommon.ini
            $path = '../config/simulator/grids/' . $grid_name . '.ini';
            if (isset($content['DefaultGridCommonIni']))
            {
                $path = $content['DefaultGridCommonIni'];
            }
            $data = \Phposdck\Utils::readIni($path);
            // fill values from submitted
            if (isset($content['GridCommon.ini']))
            {
                foreach ($content['GridCommon.ini'] as $k => $v)
                {
                    $data[$k] = $v;
                }
            }
            // remove database params
            unset($data['DatabaseService']);
            $inis['GridCommon.ini'] = $data;
        }

        // get Custom.ini values if any
        if (isset($content['Custom.ini']))
        {
            $inis['Custom.ini'] = $content['Custom.ini'];
        }

        // set ini files
        $this->IniFiles = $inis;
    }

    public function isValid()
    {
        // Uuid
        if (is_null($this->Uuid))
        {
            throw new \Exception('Wrong Uuid');
        }
        // Port
        if (is_null($this->Port) ||
                $this->Port < $GLOBALS['config']['Ports']['first_port'] ||
                $this->Port > $GLOBALS['config']['Ports']['last_port'])
        {
            throw new \Exception('Wrong Port');
        }
        // OwnerName
        if (is_null($this->OwnerName) || empty($this->OwnerName))
        {
            throw new \Exception('Wrong OwnerName');
        }
        // OwnerUuid
        if (is_null($this->OwnerUuid) ||
                $this->OwnerUuid == \Phposdck\Uuid::UUID_ZERO ||
                !\Phposdck\Uuid::check($this->OwnerUuid))
        {
            throw new \Exception('Wrong OwnerUuid');
        }
        // simulator owner folder
        $SimulatorOwnerFolder = $GLOBALS['config']['Base']['host_folder'] . '/sims/' . $this->OwnerUuid;
        if (!is_dir($SimulatorOwnerFolder))
        {
            throw new \Exception('Wrong simulator owner folder');
        }
        // simulator folder
        $sim_folder = $SimulatorOwnerFolder . '/sims/' . $this->Uuid;
        if (!is_dir($sim_folder))
        {
            throw new \Exception('Wrong simulator folder ');
        }
        // archive folder
        if (!is_dir($SimulatorOwnerFolder . '/archives'))
        {
            throw new \Exception('Wrong archives folder');
        }
        // log folder
        if (!is_dir($sim_folder . '/log'))
        {
            throw new \Exception('Wrong log folder');
        }
        // config folder
        if (!is_dir($sim_folder . '/config'))
        {
            throw new \Exception('Wrong config folder');
        }
        // config-include folder
        if (!is_dir($sim_folder . '/config-include'))
        {
            throw new \Exception('Wrong config-include folder');
        }
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

}
