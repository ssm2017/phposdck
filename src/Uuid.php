<?php

namespace Phposdck;

class Uuid
{

    const UUID_ZERO = '00000000-0000-0000-0000-000000000000';

    /**
     * 
     * @return uuid
     */
    static function create()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    /**
     * 
     * @param type $uuid
     * @return boolean
     */
    static function check($uuid)
    {
        if (preg_match("/^[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}$/", $uuid))
        {
            return TRUE;
        }
        return FALSE;
    }

}
