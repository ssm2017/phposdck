#!/bin/bash

docker inspect --format='{{range $p, $conf := .HostConfig.PortBindings}} {{$p}} -> {{(index $conf 0).HostPort}} {{end}}' $1
